import {Component, OnInit} from '@angular/core';
import {TodoItemComponent} from './todo-item/todo-item.component';
import {TodoTextInputComponent} from './todo-textinput/todo-textinput.component';

@Component({
    selector: 'todo-list',
    directives: [TodoTextInputComponent, TodoItemComponent],
    template: require('./todo-list.component.html'),
    styles: [require('./todo-list.component.css')]
})
export class TodoListComponent implements OnInit{
    constructor() {
        console.log("waaat")
    }
    ngOnInit() {
        console.log("waaat")
    }

    onAdd(newTodoText: string){
        console.log(newTodoText);
    }
}
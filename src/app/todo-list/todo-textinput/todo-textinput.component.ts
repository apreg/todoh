import {Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'todo-textinput',
    template: require('./todo-textinput.component.html'),
    styles: [require('./todo-textinput.component.css')]
})
export class TodoTextInputComponent {
    //@Input() usernameControl: string;
    //@Output() onAdd = new EventEmitter<string>();
    public newTodoText = 'whaat';

    constructor() {
        console.log('sssssss');
    }

    addTodo () {
        if (this.newTodoText.trim().length) {
            //this.onAdd.emit(this.newTodoText.trim());
            this.newTodoText = '';
        }
    }
}
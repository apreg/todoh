export class TodoItem {
    constructor(
        public text: string,
        public id: string) { }
}
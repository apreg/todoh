var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shared_1 = require('../shared');
var shared_2 = require("../shared");
var router_deprecated_1 = require("@angular/router-deprecated");
var LoginComponent = (function () {
    function LoginComponent(router, authService) {
        this.router = router;
        this.authService = authService;
        this.user = new shared_2.User('', '');
        this.errorMsg = '';
    }
    LoginComponent.prototype.login = function () {
        if (!this.authService.login(this.user)) {
            this.errorMsg = 'Failed to login';
        }
        else {
            this.router.navigate(['TodoList']);
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            providers: [shared_1.AuthService],
            template: require('./login.component.html'),
            styles: [require('./login.component.css')]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, shared_1.AuthService])
    ], LoginComponent);
    return LoginComponent;
})();
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map
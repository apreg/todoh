import {Component} from '@angular/core'
import {AuthService} from '../shared/auth.service'
import {User} from "../shared/user.model";
import {Router} from "@angular/router-deprecated";
import {ControlGroup, FormBuilder, Validators, Control, FORM_DIRECTIVES} from "@angular/common";
import {OnInit} from '@angular/core';

@Component({
    selector: 'login',
    providers: [AuthService],
    directives:[FORM_DIRECTIVES],
    template: require('./login.component.html'),
    styles: [require('./login.component.css')]
})
export class LoginComponent implements OnInit{

    user:User;
    errorMsg = '';
    form:ControlGroup;
    constructor(private router:Router, private authService:AuthService, private builder:FormBuilder) {


    }

    ngOnInit() {
        let usernameControl = new Control('', Validators.compose([
            Validators.required,
            Validators.minLength(3)
        ]));
        let passwordControl = new Control('', Validators.compose([
            Validators.required,
            Validators.minLength(3)
        ]));

        this.form =new ControlGroup({
            username: usernameControl,
            password: passwordControl
        });

        this.user.username = usernameControl.value;
        this.user.password = passwordControl.value;
    }

    login() {
        if (!this.authService.login(this.user)) {
            this.errorMsg = 'Failed to login';
        } else {
            this.router.navigate(['TodoList']);
        }
    }
}
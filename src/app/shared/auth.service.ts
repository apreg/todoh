import { Injectable } from '@angular/core';
import {User} from './user.model';

@Injectable()
export class AuthService {
    public currentUser:User;
    constructor() {
        // https://github.com/Microsoft/TypeScript/issues/6373
        this.currentUser = {username: null, password: null};
    }



    login(user:User) {
        let storedUser = JSON.parse(localStorage.getItem(user.username));
        if (!storedUser) {
            //console.log("debugger sux");
            this.storeUser(user);
            this.currentUser = user;
            return true;
        } else{
            if (storedUser.password === user.password){
                this.currentUser = user;
                return true;
            }
        }
        return false;
    }

    storeUser(user:User){
        localStorage.setItem(user.username, JSON.stringify(user));
    }


}

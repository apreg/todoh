import { Component } from '@angular/core';
import { RouteConfig, Router, ROUTER_DIRECTIVES  } from '@angular/router-deprecated';
import {LoginComponent} from './login/login.component';
import '../assets/style/styles.css';
import 'normalize.css';
import {TodoListComponent} from "./todo-list/todo-list.component";
@Component({
    selector: 'app',
    template: require('./app.component.html'),
    styles: [require('./app.component.css')],
    directives: [ROUTER_DIRECTIVES]
})
@RouteConfig([
    {path: '/', name: 'Login', component: LoginComponent, useAsDefault: true},
    {path: '/todo-list', name: 'TodoList', component: TodoListComponent}
])
export class App {
}

import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { App } from './app/app.component';
import { ROUTER_PROVIDERS } from '@angular/router-deprecated';


export function main():Promise<any> {
    return bootstrap(App, [ROUTER_PROVIDERS])
        .catch(err => console.error(err));
}

if ('development' === process.env.ENV) {
    main();
} else if (process.env.ENV === 'production') {
    enableProdMode();
    document.addEventListener('deviceready', () => main(), false);
}

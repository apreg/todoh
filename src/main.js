var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var core_1 = require('@angular/core');
var app_component_1 = require('./app/app.component');
var router_deprecated_1 = require('@angular/router-deprecated');
function main() {
    return platform_browser_dynamic_1.bootstrap(app_component_1.App, [router_deprecated_1.ROUTER_PROVIDERS])
        .catch(function (err) { return console.error(err); });
}
exports.main = main;
if ('development' === process.env.ENV) {
    main();
}
else if (process.env.ENV === 'production') {
    core_1.enableProdMode();
    document.addEventListener('deviceready', function () { return main(); }, false);
}
//# sourceMappingURL=main.js.map